# Nightschool
![image](./repo_pic.jpg)  

My online hub for learning and practice

Career  
[Clean Code: The Good, the Bad and the Ugly](https://gerlacdt.github.io/blog/posts/clean_code/)  
[Pragmatic Engineering](https://blog.pragmaticengineer.com/)
[Chelsea Troy](https://chelseatroy.com/category/professional-development/)

Podcasts
[Greater Than Code](https://www.greaterthancode.com/): Podcast on the human side of software development and technology.  

Git  
[Git Branching and Merging: A Step-By-Step Guide](https://www.varonis.com/blog/git-branching#merging)  

Artifacts
[OCI Artifacts](https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_oci_tutorial.html#configure-the-manifest-repository-to-create-an-oci-artifact)  

Testing  
[Testing](https://github.com/sindresorhus/awesome?tab=readme-ov-file#testing)  
[ChatOps](https://github.com/exAspArk/awesome-chatops)  
