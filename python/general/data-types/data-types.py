#!/usr/bin/env python3

ball = "football"
quarters = 4
gameday_temperature = 72.5
stars = ["OBJ", "Tom Brady", "Aaron Donald"]
teams = {"Name" : ["Cowboys", "Falcons", "Lions"],
        "Location" : ["Dallas", "Atlanta", "Detroit"]}
is_soccer = False

print(type(ball))
print(type(quarters))
print(type(gameday_temperature))
print(type(stars))
print(type(teams))
print(type(is_soccer))