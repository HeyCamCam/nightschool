#!/usr/bin/env python3

import random

class Being:
    being_name = ""
    being_home = ""
    being_job = ""

    def __init__(self, being_name, being_home, being_job):
        self.name = being_name
        self.home = being_home
        self.job = being_job

    def get_name(self):
        print(f"My name is {self.name}.")

    def get_home(self):
        print(f"I live {self.home}.")

    def get_job(self):
        print(f"I am {self.job}.")

class Animal(Being):
    animal_noise = ""

    def __init__(self, animal_name, animal_home, animal_job, animal_noise):
        super().__init__(animal_name, animal_home, animal_job)
        self.animal_noise = animal_noise

    def make_noise(self):
        print(f"I go {self.animal_noise}")

class Pokemon(Being):
    pokemon_type = ""

    def __init__(self, pokemon_name, pokemon_home, pokemon_job, pokemon_type):
        super().__init__(pokemon_name, pokemon_home, pokemon_job)
        self.pokemon_type = pokemon_type

    def get_pokemon_type(self):
        print(f"I am a {self.pokemon_type} type pokemon.")

    def get_pokemon_fact(self):
        pokemon_facts = ["Pokemon means 'Pocket Monsters'.", "Pikachu directly translates as 'Sparkly mouse noise'.", "Over 30 billion Pokémon cards have been sold.", "Only Mario has sold more video games than Pokémon.", "Gold and Silver were meant to be the last Pokémon games, but people wanted more.", "The colours of the Pokeballs are actually based on Cambell's soup cans.", "Ash's Pokémon was originally meant to be a Clefairy, not Pikachu.", "Many Pokémon are based on traditional Japanese stories and legends.", "The smallest Pokémon is the Flabébé.", "The largest Pokemon is Eternatus.", "Some of the rarest Pokémon cards are worth over $100,000.", "Pokémon started as a video game in 1996.", "Pokemon is all about friendship and teamwork."]
        print(f"Pokemon Fact: {random.choice(pokemon_facts)}")


if __name__ == '__main__':
    human_1 = Being("Michael Jordan", "North Carolina", "a Pro Basketball Player")
    human_1.get_name()
    human_1.get_home()
    human_1.get_job()
    print("")

    animal_1 = Animal("Cow", "on a farm", "a milk maker", "Moooooooooo!")
    animal_1.get_name()
    animal_1.get_home()
    animal_1.get_job()
    animal_1.make_noise()
    print("")

    pokemon_1 = Pokemon("Charizard", "near volcanos", "a protector", "fire", )
    pokemon_1.get_name()
    pokemon_1.get_home()
    pokemon_1.get_job()
    pokemon_1.get_pokemon_type()
    pokemon_1.get_pokemon_fact()
