# Flask

Demo of a flask application

Run command
docker build -t cam/flask --build-arg PYTHON_VERSION=python:3.10.8-bullseye -f Dockerfile.flask . && ./projects/flask/run-flask.sh && sleep 5 && curl 192.168.0.2:8000

# Pytest  
[Anatomy of a test](https://docs.pytest.org/en/stable/explanation/anatomy.html#test-anatomy)  
  1. Arrange
    - [Fixtures](https://docs.pytest.org/en/stable/explanation/fixtures.html)  
  2. Act
  3. Assert
  4. Cleanup
