#!/usr/bin/env python3

# Specify a type on to a variable.

# Set variables
# Integers
x = int(1)
y = int(2.8)
z = int("3")

print(type(x))
print(y)
print(z)

# Floats
x = float(1)
y = float(2.8)
z = float("3")

print(type(x))
print(y)
print(z)

# Strings
x = str(1)
y = str(2.8)
z = str("3")

print(type(x))
print(y)
print(z)