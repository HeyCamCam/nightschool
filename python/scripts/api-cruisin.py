#!/usr/bin/env python3

import httpx

# Free APIs
# https://apipheny.io/free-api/
# https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis/

class api:
    cats = 'https://catfact.ninja/fact'
    dogs = 'https://dog.ceo/api/breeds/image/random'
    coindesk = 'https://api.coindesk.com/v1/bpi/currentprice.json'
    bored = 'https://www.boredapi.com/api/activity'
    agify = 'https://api.agify.io?name='
    genderize = 'https://api.genderize.io?name='
    ipinfo = 'https://ipinfo.io/' + ip_addr + '/geo'
    jokes = 'https://official-joke-api.appspot.com/random_joke'
    randomuser = 'https://randomuser.me/api/'

r = httpx.get('https://catfact.ninja/fact')
data = r.text

print(data)
