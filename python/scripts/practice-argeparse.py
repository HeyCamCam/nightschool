#!/usr/bin/env python3

import argparse

def parse_args(arg_list: list[str] | None):
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', '-f', help='File to be analyzed')
    files = parser.parse_args(arg_list)
    return files

def main(arg_list: list[str] | None = None):
    files = parse_args(arg_list)
    print(files.file)

if __name__ == '__main__':
    main()
