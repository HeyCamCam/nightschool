#!/usr/bin/env python3

import os
import json
import shutil
import urllib3
import subprocess


url = "https://catfact.ninja/fact"
http_pool = urllib3.connection_from_url(url)
cat_raw_data = http_pool.urlopen("GET", url)
cat_fact_output = json.loads(cat_raw_data.data.decode("utf-8"))
cat_fact = cat_fact_output["fact"]

url = "https://official-joke-api.appspot.com/random_joke"
data = urllib3.PoolManager().request("GET", url).data
setup = json.loads(data)["setup"]
punchline = json.loads(data)["punchline"]
random_joke = f"{setup} {punchline}"


def main():
    env = {"GIT_SSH_COMMAND": f"ssh -i ~/.ssh/id_rsa_gitlab"}

    if os.path.isdir("/tmp/auto-powered"):
        print("The /tmp/auto-powered directory exists. Removing.")
        shutil.rmtree("/tmp/auto-powered")

    print("Git - Cloning repository")
    subprocess.run(
        [
            "git",
            "clone",
            "git@gitlab.com:HeyCamCam/auto-powered.git",
            "/tmp/auto-powered",
        ],
        check=True,
    )

    print("Git - Updating README")
    with open("/tmp/auto-powered/README.md", "w") as readme:
        subprocess.run(["echo", "Cat Fact -"], stdout=readme)
        subprocess.run(["echo", cat_fact], stdout=readme)
        subprocess.run(["echo", ""], stdout=readme)
        subprocess.run(["echo", "Random Joke -"], stdout=readme)
        subprocess.run(["echo", random_joke], stdout=readme)

    print("Git - Adding README.md")
    subprocess.run(["git", "add", "README.md"], cwd="/tmp/auto-powered")

    print("Git - git commit -m message")
    subprocess.run(["git", "commit", "-m", cat_fact], cwd="/tmp/auto-powered")

    print("Git - git push")
    subprocess.run(["git", "push"], env=env, cwd="/tmp/auto-powered")

    print("Filesystem - Removing repository")
    shutil.rmtree("/tmp/auto-powered")


if __name__ == "__main__":
    main()
