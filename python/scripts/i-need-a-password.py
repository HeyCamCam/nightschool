#!/usr/bin/env python3

import sys
import string
import random

def gen_pass(total_characters):
    characters = string.ascii_lowercase + string.ascii_uppercase + string.digits
    password = ''.join(random.choice(characters) for _ in range(total_characters))
    print(password)

if __name__ == '__main__':
    gen_pass(int(sys.argv[1]))
