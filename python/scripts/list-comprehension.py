#!/usr/bin/env python3

def generate_list():
    '''List Comprehension\n[expression for item in iterable if condition]\n'''
    squares = [i**2 for i in range(5000)]
    print(squares)

if __name__ == '__main__':
    print(generate_list.__doc__)
    generate_list()
