#!/usr/bin/env python3

import os
import sys
import gitlab

# command line syntax - ./script.py theFunction theArgument
# Gitlab Python API documentation - https://python-gitlab.readthedocs.io/en/stable/api-objects.html

# set global variables and values
# configure parser to determine which function to run
args = sys.argv

# configure Gitlab authentication using private token
gl = gitlab.Gitlab(private_token=os.environ.get('GITLABBIN_ACCESS_TOKEN'), per_page=100)
gl.auth()

def list_all_runners():
    '''List owned runners'''
    all_runners = gl.runners.list(all=True)
    
    try:
        for index, runner in enumerate(all_runners):
            print(runner.id)
    except Exception as e:
        pass

def list_active_runners():
    '''List owned runners with a filter'''
    active_runners = gl.runners.list(scope='active')
    
    try:
        for index, runner in enumerate(active_runners):
            print(runner.id)
    except Exception as e:
        pass

def list_inactive_runners():
    '''List owned runners that are inactive'''
    offline_runners = gl.runners.list(scope='offline')
    
    try:
        for index, runner in enumerate(offline_runners):
            print(runner.id)
    except Exception as e:
        pass

def get_runner_info(runner_id):
    '''Get runner information using the runner id'''
    this_runner = gl.runners.get(runner_id)
    
    try:
        for index, runner in enumerate(runner_id):
            this_runner.pprint()
    except Exception as e:
        pass

def delete_runner(runner_id):
    offline_runners = gl.runners.list(scope='offline')

    for index, runner in enumerate(offline_runners):
        print(f"Removing runner {runner.description}, {runner.id}")
        gl.runners.delete(runner.id)

def create_project(project_name):
    try:
        gl.projects.create({'name': project_name})
        print(f"Project {project_name} was created")
    except Exception as e:
        print(f'Error with creating project {project_name}')

def delete_project(project_ids: str) -> str:
    try:
        for project_id in project_ids:
            gl.projects.get(project_id).delete()
            print(f'Project {project_id} was deleted')
    except Exception as e:
        print(f'Error deleting project {project_id}')

if __name__ == '__main__':
    # call a function and pass an argument
    #globals()[args[1]](*args[2:])
    globals()[args[1]](args[2:])
