#!/usr/bin/env python3

import lxml.html
import httpx
from bs4 import BeautifulSoup

def dhts():
    print("DHTS IT Jobs")
    url = "https://careers.duke.edu/search/?createNewAlert=false&q=&optionsFacetsDD_customfield2=&optionsFacetsDD_customfield1=&optionsFacetsDD_location=&optionsFacetsDD_dept=Information+Technology&optionsFacetsDD_shifttype="

    html = httpx.get(url)
    soup = BeautifulSoup(html, 'html.parser')

    # Find all elements (jobs) with the class "jobTitle-link"
    jobs = set(soup.find_all("a", class_="jobTitle-link"))
    for job in jobs:
        job_link = job.get('href')
        if job_link:
            print(f"{job.text} - https://careers.duke.edu{job_link}")

    print("")

def emory():
    print("Emory IT Jobs")
#print("Emory IT Jobs")
#emory_url = "https://staff-emory.icims.com/jobs/search?ss=1&searchKeyword=linux&searchRelation=keyword_all&searchCategory=17346"
# Find all elements (jobs) with the class "jobTitle-link"
#jobs = set(soup.find_all("a", class_="jobTitle-link"))
#for job in jobs:
#    job_link = job.get('href')
#    if job_link:
#        print(f"{job.text} - https://careers.duke.edu{job_link}"

if __name__ == '__main__':
    dhts()
    emory()
