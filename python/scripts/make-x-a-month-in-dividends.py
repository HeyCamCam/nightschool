#!/usr/bin/env python3

import sys

def monthly_dividend_amount(monthly_amount, dividend_yield):
    """To calculate the amount of investment required, first take $100 a month times 12 months.  That gives us $1,200 in annual dividend income. Then take that $1,200 and divide it by your target dividend yield.  4%, in this example.  Thus, $1,200 divided 4% which is .04 gives you $30,000. As a result, $30,000 is much you will need to invest to make $100 a month in dividends assuming your portfolio yields 4%. https://dividendsdiversify.com/100-a-month-in-dividends/"""
    annual_dividend_income = monthly_amount * 12
    target_dividend_yield = dividend_yield / int(100)
    return print(round(annual_dividend_income / target_dividend_yield))

if __name__ == '__main__':
    monthly_dividend_amount(int(sys.argv[1]), int(sys.argv[2]))
