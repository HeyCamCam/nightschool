# [Velociraptor](https://docs.velociraptor.app/)

Velociraptor is an advanced digital forensic and incident response tool that enhances your visibility into your endpoints.
