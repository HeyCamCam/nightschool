#!/usr/bin/env python3

import re
import csv
import requests

# Url
csv_url = "https://www.cboe.com/available_weeklys/get_csv_download"

# Download csv file 
response = requests.get(csv_url)
with open("weekly-options.csv", "wb") as file:
  file.write(response.content)

# Sanitization
# Remove blank lines from file by opening it again in read-write mode
with open("weekly-options.csv") as reader, open("weekly-options.csv", "r+") as writer:
  for line in reader:
    if line.strip():
      writer.write(line)
  writer.truncate()

# Remove everything before the "Available Weeklys - Exchange Traded Products (ETFs and ETNs)" line
with open("weekly-options.csv") as reader:
  for line in reader:
    re.sub(Available, 'REDACTED', str(tfstate))
    # Retrieve stock tickers
    #line = list(line.split(","))
    #print(line[0])
    # Retrieve list of information
    #line = list(line.split(","))
    #if len(line) > 1:
    #  print(line)
    # Find Available string and delete everyting before it
    #line = list(line.split(","))
    #available = "Available"
    #if available in line:
    #  location = line.find(available)
    #  new_line= line[0:location]
    #  print(new_line)   
