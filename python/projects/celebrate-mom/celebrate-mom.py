#!/usr/bin/env python3

import os
import requests

# Set X Rapid API key from environment variable
XRapidAPIKey = os.environ['X_RAPIDAPI_KEY']

url = "https://arik-e-cards.p.rapidapi.com/"

payload = "subject=Happy%20Birthday%20Mom!&cardTemplateId=3309&message=Happy%20Birthday!&toAddress=toliti9820%40tagbert.com&to=Mom&fromAddress=veyecec698%40syswift.com&from=Son"
headers = {
	"content-type": "application/x-www-form-urlencoded",
	"Accept": "application/json",
	"X-RapidAPI-Key": XRapidAPIKey,
	"X-RapidAPI-Host": "arik-e-cards.p.rapidapi.com"
}

response = requests.request("POST", url, data=payload, headers=headers)

# To Do - Attach to email
