# Python

# [Awesome Python](https://github.com/vinta/awesome-python)  
  - [Awesome asyncio](https://github.com/timofurrer/awesome-asyncio)  

# Python Specifications  
[Static Typing with Python](https://typing.readthedocs.io/en/latest/)  

# Libraries  
[PyMOTW-3](https://pymotw.com/3/): Python 3 Module of the Week  
[Fluent Python](https://www.oreilly.com/library/view/fluent-python-2nd/9781492056348/)  

# Design Patterns  
[Python Patterns](https://github.com/faif/python-patterns)  
[Python Factory Patterns](https://dagster.io/blog/python-factory-patterns)  
[Python Design Patterns](https://python-patterns.guide/)  
[Clean Architectures in Python](https://www.thedigitalcatbooks.com/pycabook-introduction/)  
[Architecture Patterns with Python](https://www.cosmicpython.com/book/preface.html)  
[Design Patterns in Python: A Series](https://medium.com/@amirm.lavasani/design-patterns-in-python-a-series-f502b7804ae5)  
  
  &nbsp;&nbsp; Creational Patterns  
  - [Singleton Pattern](https://python.plainenglish.io/design-patterns-in-python-singleton-5095a4c14f)  
  - [Factory Method Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-factory-method-1882d9a06cb4)  
  - [Design Patterns in Python: A Series](https://medium.com/@amirm.lavasani/design-patterns-in-python-a-series-f502b7804ae5)  
  - [Abstract Factory Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-abstract-factory-2dcae06e5d29)   
  - [Builder Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-builder-0732552324b1)  
  - [Prototype Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-prototype-6aeeda10f41e)  

  &nbsp;&nbsp; Structural Patterns  
  - [Adapter Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-adapter-58eb7cc11474): Converts the interface of a class into another interface that clients expect, enabling classes with incompatible interfaces to work together.  
  - [Bridge Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-bridge-c34f3fcdd2eb): Decouples an abstraction from its implementation, allowing both to evolve independently.  
  - [Composite Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-composite-09eba144f65e): Composes objects into tree structures to represent part-whole hierarchies, making it easier to work with individual objects and compositions.  
  - [Decorator Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-decorator-c882c0db6501): Dynamically adds responsibilities to objects, providing a flexible alternative to subclassing for extending functionality.  
  - [Facade Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-facade-0043afc9aa4a): Provides a simplified interface to a complex subsystem, making it easier to use and understand.  
  - [Flyweight Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-flyweight-e7a7334f82b1): Shares instances of objects to support large numbers of fine-grained objects efficiently.  
  - [Proxy Pattern](https://medium.com/@amirm.lavasani/design-patterns-in-python-proxy-bd04fedbe83d): Provide a substitute or placeholder for another object to control access to the original object.  

# Testing  
[Awesome PyTest](https://github.com/augustogoulart/awesome-pytest)  

# Blogs  
  - [Design Patterns](https://refactoring.guru/design-patterns)  
  - [Refactoring](https://refactoring.guru/refactoring)
