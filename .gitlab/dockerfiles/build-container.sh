#!/usr/bin/env bash

docker build --build-arg UID="$(id -u)" --build-arg GID="$(id -g)" --build-arg UNAME="$(whoami)" . -t nightschool -f Dockerfile
