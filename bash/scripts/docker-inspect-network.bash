#!/bin/bash

docker container inspect -f="{{json .NetworkSettings}}" $1 | /usr/bin/python3 -m json.tool
