#!/bin/bash

# Use native linux tools to generate a random string defined by a length you supply.

# How do I run this?
# ./generate-string.sh 16

tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c "$@"; echo
