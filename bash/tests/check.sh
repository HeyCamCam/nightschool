#!/bin/bash

isThisRoot()
{
   if [ `id -u` == 0 ]
   then
      echo "This appears to be running as root"
      exit
   fi
}
