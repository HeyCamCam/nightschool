package main

import (
	"fmt"
	"time"
)

func PrepShip() {
        fmt.Println("Prepping spaceship")
	time.Sleep(1 * time.Second)
        fmt.Println("Adjusting pilot seat")
	time.Sleep(1 * time.Second)
        fmt.Println("Checking fuel level")
	time.Sleep(1 * time.Second)
        fmt.Println("Turning on A/C")
	time.Sleep(1 * time.Second)
	fmt.Printf("\n")
}

func StartShip() {
	fmt.Println("Starting spaceship")
	time.Sleep(1 * time.Second)
	fmt.Println("Starting left engine")
        time.Sleep(1 * time.Second)
        fmt.Println("Starting right engine")
        time.Sleep(1 * time.Second)
        fmt.Println("Starting navigation and communication systems")
        time.Sleep(1 * time.Second)
	fmt.Printf("\n")
}

func FlyTo(FlightStartDestination, FlightEndDestination string) {
	fmt.Println("Plugging in coordinates for", FlightStartDestination)
        fmt.Println("Ship is enroute to", FlightEndDestination)
	time.Sleep(5 * time.Second)
	fmt.Printf("\n")
}

func AnnounceArrival() {
        fmt.Println("Ship has reached the destination")
	time.Sleep(1 * time.Second)
	fmt.Printf("\n")
}

func ShutdownShip() {
        fmt.Println("Shutting down spaceship")
	time.Sleep(1 * time.Second)
        fmt.Println("Shutting down left engine")
	time.Sleep(1 * time.Second)
        fmt.Println("Shutting down right engine")
	time.Sleep(1 * time.Second)
        fmt.Println("Shutting down navigation and communication systems")
	time.Sleep(1 * time.Second)
	fmt.Printf("\n")
}

func TravelDebrief() {
	fmt.Println("Traveled from: ")
	fmt.Println("Total time: ")
	fmt.Println("Total fuel consumed: ")
	fmt.Printf("\n")
}

func main() {
	PrepShip()
	StartShip()
	FlyTo("Earth", "Jupiter")
	AnnounceArrival()
	ShutdownShip()
	TravelDebrief()
}
