package main

import (
	"fmt"
	"time"
)

func prepShip() {
        fmt.Println("--Prepping spaceship--")
	time.Sleep(1 * time.Second)
        fmt.Println("Adjusting pilot seat")
	time.Sleep(1 * time.Second)
        fmt.Println("Checking fuel level")
	time.Sleep(1 * time.Second)
        fmt.Println("Turning on A/C")
	time.Sleep(1 * time.Second)
	fmt.Printf("\n")
}

func startShip() {
	fmt.Println("--Starting spaceship--")
	time.Sleep(1 * time.Second)
	fmt.Println("Starting left engine")
        time.Sleep(1 * time.Second)
        fmt.Println("Starting right engine")
        time.Sleep(1 * time.Second)
        fmt.Println("Starting navigation and communication systems")
        time.Sleep(1 * time.Second)
	fmt.Println("Setting up Google Maps")
        time.Sleep(1 * time.Second)
	fmt.Printf("\n")
}

func flyTo(FlightStartDestination, FlightEndDestination string) {
        fmt.Println("--Flight--")
        time.Sleep(1 * time.Second)
        fmt.Println("Navigating ship to takeoff area")
        time.Sleep(2 * time.Second)
        fmt.Println("Ship is taking off")
        time.Sleep(2 * time.Second)
        fmt.Println("Ship is en route to", FlightEndDestination)
	time.Sleep(5 * time.Second)
	fmt.Println("Ship has reached", FlightEndDestination)
        time.Sleep(2 * time.Second)
	fmt.Println("Landing ship")
        time.Sleep(2 * time.Second)
	fmt.Println("Navigating to parking area")
        time.Sleep(2 * time.Second)
	fmt.Printf("\n")
}

func shutdownShip() {
        fmt.Println("--Shutting down spaceship--")
	time.Sleep(1 * time.Second)
        fmt.Println("Shutting down left engine")
	time.Sleep(1 * time.Second)
        fmt.Println("Shutting down right engine")
	time.Sleep(1 * time.Second)
        fmt.Println("Shutting down navigation and communication systems")
	time.Sleep(1 * time.Second)
        fmt.Println("Exiting spaceship")
	fmt.Printf("\n")
}

func main() {
	var FlightStartDestination = "Earth"
	var FlightEndDestination = "Jupiter"

	prepShip()
	startShip()
	flyTo(FlightStartDestination, FlightEndDestination)
	shutdownShip()
}
