# Go  

[Awesome Go](https://github.com/avelino/awesome-go)  

# Tutorial Sites
[A Tour of Go](https://go.dev/tour/welcome/1)  
[Golang Bot](https://golangbot.com/learn-golang-series/)  
[How to Write Go Code](https://go.dev/doc/code)  

# Project Organization
[Go directories explained](https://github.com/golang-standards/project-layout?tab=readme-ov-file#go-directories)  

Modules  
[Creating a Go module](https://go.dev/doc/tutorial/create-module)  
[Organizing a Go module](https://go.dev/doc/modules/layout)  

# Blogs  
[Alex Edwards](https://www.alexedwards.net/blog)  
[Aurélie Vache](https://dev.to/aurelievache/series/26234)
